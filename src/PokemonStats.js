import styles from './PokemonStats.module.css';

export default function PokemonStats({ stats }) {
    return (
        <div className={`${styles.root}`}>
            <h3>Stats</h3>
            <dl className={ styles.list }>
                {
                    stats && stats.map((stat, i) => (
                        <div key={ i } className={ styles.listItem }>
                            <dt className={ styles.statName }>{ stat.name }</dt>
                            <dd className={ styles.statValue }>{ stat.value }</dd>
                        </div>
                    ))
                }
            </dl>
        </div>
    );
}
