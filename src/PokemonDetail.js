import { useState } from 'react';
import Button from './Button';
import styles from './PokemonDetail.module.css';
import PokemonStats from './PokemonStats';

export default function PokemonDetail({ id, name, image, types, stats, onSave, className }) {
    const [pokemonConfig, setPokemonConfig] = useState({
        name,
        image,
        types,
        id,
    });

    return (
        <div className={ `${styles.root} ${className}` }>
            <div className={ `${styles.pokemon}` }>
                <img src={ image } alt={ name } />
                <div className={ `${styles.name}` }>
                    {
                        name
                    }
                </div>
                <Button onClick={() => onSave(pokemonConfig)}>SAVE</Button>
            </div>
            <PokemonStats stats={ stats } />
            {/* <PokemonMoves moves={ pokemonConfig.moves } /> */}
            {/* <PokemonMovesSelector onChange={(move) => setPokemonConfig({ ...pokemonConfig, move }))} /> */}
        </div>
    );
}
