import { useQuery } from 'graphql-hooks';
import styles from './PokemonSelector.module.css';
import { FETCH_ALL } from './api/queries';
import AutoComplete from './AutoComplete';

export default function PokemonSelector({ className, onChange = () => {} }) {
    const { loading, error, data } = useQuery(FETCH_ALL, { variables: { first: 151 } });

    const items = data?.pokemons;

    if (loading) return 'Loading...';
    if (error) return 'Something bad happened';

    return (
        <div className={ `${className} ${styles.root}` }>
            <AutoComplete items={ items } label="Select a pokemon" onChange={ onChange } />
        </div>
    );
}
