import { useState } from "react";

export default function usePokemonSquadSelection(size = 6) {
    const [pokemonSquad, setPokemonSquad] = useState([]);

    function addPokemonToSquad(pokemon) {
        return pokemonSquad.length < size ? setPokemonSquad([...pokemonSquad, pokemon]) : false;
    }

    function removePokemonFromSquad(index) {
        return setPokemonSquad(pokemonSquad.filter((pokemon, i) => i !== index));
    }

    return {
        pokemonSquad,
        addPokemonToSquad,
        removePokemonFromSquad,
    };
}
