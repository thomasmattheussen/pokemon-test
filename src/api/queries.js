const FETCH_ALL = `query AllPokemons($first: Int) {
    pokemons:Pokemons(first: $first) {
        id
        name
    }
}`;

const FETCH_SINGLE = `query SinglePokemon($name: String!) {
    pokemon:Pokemon(name: $name) {
        id
        name
        image
        abilities {
            name
        }
        stats {
            name
            value
        }
        types {
            name
        }
        moves {
            learnMethod
            name
        }
    }
}
`;

export { FETCH_ALL, FETCH_SINGLE };
