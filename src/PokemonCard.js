import styles from './PokemonCard.module.css';

export default function PokemonCard({ pokemon }) {
    const { name, types, moves, image } = pokemon;

    return (
        <div className={`${styles.root} ${styles[`type--${ types[0].name }`]}`}>
            <img src={ image } alt={ name } className={ styles.media } />
            { name }
            <ul className={ styles.moves }>
                {
                    moves && moves.map((move, i) => (
                        <li key={ i } className={ styles.move }>{ move.name }</li>
                    ))
                }
            </ul>
        </div>
    );
}
