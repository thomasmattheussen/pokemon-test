import DeleteIcon from "./icons/Delete";
import PokemonCard from "./PokemonCard";
import styles from './PokemonSquad.module.css';

export default function PokemonSquad({ squad = [], onRemove = () => {} } = {}) {
    return (
        <div className={ styles.root }>
            <h2>Selected squad</h2>
            {
                squad.length > 0 ? (
                    <ul className={ styles.list }>
                        {
                            squad.map((pokemon, index) => (
                                <li key={ index } className={ styles.listItem }>
                                    <PokemonCard pokemon={ pokemon } />
                                    <button
                                        className={ styles.delete }
                                        onClick={ () => onRemove(index) }
                                    >
                                        <DeleteIcon width="16" height="16" />
                                        <span className={ styles.deleteLabel }>Remove from squad</span>
                                    </button>
                                </li>
                            ))
                        }
                        {
                            [...Array(6 - squad.length)].map((item, index) => (
                                <li key={ index } className={`${ styles.listItem } ${ styles.listItemEmpty }`}>
                                    empty
                                </li>
                            ))
                        }
                    </ul>
                ) : (
                    <p>Select a pokemon and click the save button to add pokemon to your squad!</p>
                )
            }
        </div>
    );
}
