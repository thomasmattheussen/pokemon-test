import Downshift from 'downshift';
import styles from './AutoComplete.module.css';

export default function AutoComplete({ onChange, items, label }) {
    return (
        <Downshift
            onChange={ onChange }
            itemToString={item => (item ? item.name : '')}
        >
            {({
                getInputProps,
                getItemProps,
                getLabelProps,
                getMenuProps,
                isOpen,
                inputValue,
                highlightedIndex,
                selectedItem,
                getRootProps,
            }) => (
                <div className={ styles.root }>
                    <label className={ styles.label } { ...getLabelProps() }>{ label }</label>
                    <div className={ styles.combobox } { ...getRootProps({}, { suppressRefError: true }) }>
                        <input
                            placeholder="Type to filter"
                            className={ styles.input }
                            { ...getInputProps() }
                        />
                    </div>
                    <div className={ styles.menuWrapper }>
                        <ul className={ styles.menu } { ...getMenuProps() }>
                            {
                                items
                                    .filter((item) => !inputValue || item.name.includes(inputValue))
                                    .map((item, index) => (
                                        <li
                                            className={`
                                                ${styles.menuItem}
                                                ${highlightedIndex === index ? styles.menuItemHighlighted : '' }
                                                ${selectedItem === item ? styles.menuItemSelected : '' }
                                            `}
                                            {...getItemProps({
                                                key: item.id,
                                                index,
                                                item,
                                            })}
                                        >
                                            {item.name}
                                        </li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            )}
        </Downshift>
    );
}
