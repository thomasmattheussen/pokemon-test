import styles from './Button.module.css';

export default function Button({ className, children, ...otherProps }) {
    return (
        <button className={ `${styles.root} ${className}` } { ...otherProps }>
            { children }
        </button>
    );
}
