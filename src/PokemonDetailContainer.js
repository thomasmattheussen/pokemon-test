import { useManualQuery } from 'graphql-hooks';
import { useEffect } from 'react';

import { FETCH_SINGLE } from './api/queries';
import PokemonDetail from './PokemonDetail';

export default function PokemonDetailContainer({ name, ...otherProps }) {
    const [fetchPokemon, { loading, error, data }] = useManualQuery(FETCH_SINGLE);

    useEffect(() => {
        if (name && name !== '') {
            fetchPokemon({ variables: { name } });
        }
    }, [fetchPokemon, name]);

    if (loading) return 'Loading...';
    if (error) return 'Something bad happened';

    const pokemon = data?.pokemon;

    return (
        <>
            {
                pokemon && <PokemonDetail { ...pokemon } { ...otherProps } />
            }
        </>
    );
}
