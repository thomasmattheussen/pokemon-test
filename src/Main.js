import { useState } from 'react';
import styles from './Main.module.css';
import usePokemonSquadSelection from './hooks/usePokemonSquadSelection';
import PokemonDetailContainer from './PokemonDetailContainer';
import PokemonSelector from './PokemonSelector';
import PokemonSquad from './PokemonSquad';

export default function Main() {
    const [selectedPokemon, setSelectedPokemon] = useState();
    const { pokemonSquad, addPokemonToSquad, removePokemonFromSquad } = usePokemonSquadSelection();

    return (
        <div className={ styles.root }>
            <div className={ styles.configurator }>
                <PokemonSelector
                    onChange={({ name }) => {
                        setSelectedPokemon(name);
                    }}
                    className={ styles.selector }
                />
                <PokemonDetailContainer
                    name={ selectedPokemon }
                    onSave={ (pokemon) => addPokemonToSquad(pokemon) }
                    className={ styles.detail }
                />
            </div>
            <PokemonSquad
                squad={ pokemonSquad }
                onRemove={ removePokemonFromSquad }
            />
        </div>
    )
}
